﻿
namespace cSharpCodingAss
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    Console.WriteLine("Create Developer Menu\nEnter  Your Choice\n1.OnPayroll\n2.Oncontract basis");
                    int ch = Byte.Parse(Console.ReadLine());
                    List<Developer> dev = new List<Developer>();
                    Developer developer = new Developer();
                    for (int i = 0; i < 10; i++)
                    {
                        switch (ch)
                        {
                            case 1:
                                {
                                    developer = new OnPayRoll(); break;
                                }
                            case 2:
                                {
                                    developer = new OnContract(); break;
                                }
                            default:
                                Console.WriteLine("Enter A Valid input");
                                break;

                        }
                        //Decide.Decide1(developer);
                        developer.GetDetails();
                        dev.Add(developer);
                    }
                    //Displaying All the records With LINQ.
                    Console.WriteLine("Displaying All the records");
                    var list1 = (from x in dev select x).ToList();
                    foreach (Developer d in list1)
                        d.DisplayDetails();
                    //Displaying All the Records with d letter in name
                    Console.WriteLine("Displaying Records in which Name contains letter D");
                    var list2 = (from x in dev where x.name.Contains("D") select x).ToList();
                    foreach (Developer d in list2)
                        d.DisplayDetails();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Enter The valid Options");
            }

        }
    }


}