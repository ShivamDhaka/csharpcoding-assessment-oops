﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpCodingAss
{
    internal class OnContract:Developer
    {
        int duration;
        double charges_per_day;
        double paymentAmount;

        public OnContract() : base() { }

        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Duration of Contract:");
            duration = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Charges per Day:");
            charges_per_day = Convert.ToDouble(Console.ReadLine());
            PaymentCalculate();
        }
        public void PaymentCalculate()
        {
            paymentAmount = charges_per_day * duration;
            DisplayDetails();
        }

        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("charges are " + charges_per_day);
            Console.WriteLine("duration is " + duration);
            Console.WriteLine("Total payment done " + paymentAmount);
        }
    }
}
