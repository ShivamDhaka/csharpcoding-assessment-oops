﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpCodingAss
{
     internal class Developer
    {
        int Id;
        public string name;
        DateTime JoiningDate;
        string Project_Assigned;

        public virtual void GetDetails()
        {
            Console.WriteLine("Enter Id:");
            Id=int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Developer's Name:");
            name = Console.ReadLine();
            Console.WriteLine("Enter Joining Date:");
            JoiningDate = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Enter the Project Assigned:");
            Project_Assigned = Console.ReadLine();
        }
        public void DisplayDetails()
        {
            Console.WriteLine("The Id is {0}", Id);
            Console.WriteLine("The Name of the developer is {0}", name);
            Console.WriteLine("The Joining date of Developer is {0}", JoiningDate);
            Console.WriteLine("The Project Assigned to the developer is {0}", Project_Assigned);
        }
    }
}
