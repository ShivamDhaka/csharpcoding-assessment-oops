﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpCodingAss
{
    internal class OnPayRoll:Developer
    {
        string Dept;
        string Manager;
        double NetSalary;
        byte Exp;
        double TotalSalary;

        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Department:");
            Dept = Console.ReadLine();
            Console.WriteLine("Enter the Manager Name");
            Manager = Console.ReadLine();
            Console.WriteLine("Enter the Salary");
            NetSalary = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter the experience");
            Exp=byte.Parse(Console.ReadLine());
            Calculatepayment(NetSalary, Exp);
            DisplayDetails();
        }
        public void Calculatepayment(double netSalary, byte exp)
        {
            if (exp > 10)
            {
                TotalSalary = (((10 / 100) * NetSalary) + ((8.5 / 100) * NetSalary) - 6500+NetSalary);
            }
            else if (exp > 7 && exp < 10)
            {
                TotalSalary = (((7 / 100) * NetSalary) + ((6.5 / 100) * NetSalary) - 4100+NetSalary);
            }
            else if (exp > 5 && exp < 7)
            {
                TotalSalary = (((4.1 / 100) * NetSalary) + ((3.8 / 100) * NetSalary) - 1800+NetSalary);
            }
            else
            {
                TotalSalary = (((1.9 / 100) * NetSalary) + ((2.0 / 100) * NetSalary) - 1200+NetSalary);
            }

        }
        public void DisplayDetails()
        {
            Console.WriteLine("The Basic Salary is {0}", NetSalary);
            Console.WriteLine("The Total salary is {0}", TotalSalary);
        }
    }
}
